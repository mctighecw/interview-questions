#!/bin/sh

echo Initializing database in container...

docker exec -it interview-questions_backend-go_1 go run ./src/scripts/init_db.go
