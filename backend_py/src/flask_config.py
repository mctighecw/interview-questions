import logging
from datetime import timedelta

from src.env_config import (FLASK_ENV, DB_HOST, DB_AUTH,
    DB_NAME, DB_USER, DB_PASSWORD, SECRET_KEY)


if FLASK_ENV == "development":
    LOG_LEVEL = logging.DEBUG
    secure_cookies = False
else:
    LOG_LEVEL = logging.INFO
    secure_cookies = True

MONGODB_SETTINGS = {
    "db": DB_NAME,
    "username": DB_USER,
    "password": DB_PASSWORD,
    "host": DB_HOST,
    "authentication_source": DB_AUTH
}

JWT_TOKEN_LOCATION = ["cookies"]
JWT_SESSION_COOKIE = False
JWT_COOKIE_CSRF_PROTECT = False
JWT_COOKIE_SECURE = secure_cookies
JWT_ACCESS_COOKIE_PATH = "/api/"
JWT_REFRESH_COOKIE_PATH = "/api/auth/refresh"
JWT_ACCESS_TOKEN_EXPIRES = timedelta(days=2)
JWT_SECRET_KEY = SECRET_KEY
