from datetime import datetime, timezone

from mongoengine import Document
from mongoengine.fields import (StringField, BooleanField,
    IntField, FloatField, ListField, DateTimeField)


class User(Document):
    meta = { "collection": "users" }

    first_name = StringField()
    last_name = StringField()
    email = StringField()
    password = StringField()
    completed_questions = ListField(StringField(), default=list)
    rated_questions = ListField(StringField(), default=list)
    deleted = BooleanField(default=False)
    nr_logins = IntField(default=0)
    last_login = DateTimeField()
    registered_at = DateTimeField(default=datetime.now(timezone.utc).astimezone())
    deleted_at = DateTimeField()


class LoginInfo(Document):
    meta = { "collection": "login_info" }

    user_id = StringField()
    user_agent = StringField()
    language = StringField()
    ip = StringField()
    isp = StringField()
    connection_type = StringField()
    city = StringField()
    region = StringField()
    country = StringField()
    time_zone = StringField()
    login_timestamp = DateTimeField(default=datetime.now(timezone.utc).astimezone())


class Question(Document):
    meta = { "collection": "questions" }

    question_id = StringField()
    nr_ratings = IntField()
    overall_rating = FloatField()
    last_updated = DateTimeField(default=datetime.now(timezone.utc).astimezone())
