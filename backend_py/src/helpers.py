def get_user_info(user):
    """
    Returns the user's basic info as a dictionary.
    """
    res = {
        "id": str(user.id),
        "email": user.email,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "deleted": user.deleted,
        "nr_logins": user.nr_logins,
        "last_login": user.last_login,
        "deleted_at": user.deleted_at,
        "registered_at": user.registered_at
    }
    return res


def get_detailed_user(user):
    """
    Returns the user's detailed info as a dictionary.
    """
    res = {
        "id": str(user.id),
        "email": user.email,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "completed_questions": user.completed_questions,
        "rated_questions": user.rated_questions,
        "deleted": user.deleted,
        "nr_logins": user.nr_logins,
        "last_login": user.last_login,
        "deleted_at": user.deleted_at,
        "registered_at": user.registered_at
    }
    return res


def calculate_new_rating(rating, nr_ratings, overall_rating):
    """
    Calculates a question's new rating based on its current rating
    and the number of ratings.
    """
    new_total_ratings = nr_ratings + 1
    total = overall_rating * nr_ratings
    new_toal = total + rating
    new_overall_rating = new_toal / new_total_ratings

    return new_total_ratings, new_overall_rating
