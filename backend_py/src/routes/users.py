from flask import request, Blueprint, jsonify
from flask_cors import CORS

from src.helpers import get_user_info, get_detailed_user
from src.constants import default_headers
from src.db.models import User as UserModel

users = Blueprint("users", __name__)
CORS(users, resources=r"/api/users/*")


@users.route("/all", methods=["GET"])
def get_all():
    """
    Get all users from database.
    """
    all_users = UserModel.objects.all()

    users = []
    for u in all_users:
        users.append(get_user_info(u))

    res = jsonify({"status": "OK", "users": users})
    return res, 200, default_headers


@users.route("/one", methods=["POST"])
def get_one():
    """
    Get detailed information for one user.
    """
    req = request.json

    try:
        email = req["email"].lower()
        user = UserModel.objects.filter(email=email).first()
        user_info = get_detailed_user(user)

        res = jsonify({"status": "OK", "user": user_info})
        return res, 200, default_headers

    except:
        res = {"status": "Error", "message": "Bad request"}
        return jsonify(res), 400, default_headers
