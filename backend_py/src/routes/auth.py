from datetime import datetime, timezone
from flask import request, Blueprint, jsonify
from flask_cors import CORS
from flask_bcrypt import Bcrypt

from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, \
    jwt_refresh_token_required, get_jwt_identity, set_access_cookies, set_refresh_cookies, \
    unset_jwt_cookies)

from src.helpers import get_user_info
from src.constants import default_headers
from src.db.models import User as UserModel, LoginInfo as LoginInfoModel

bcrypt = Bcrypt()

auth = Blueprint("auth", __name__)
CORS(auth, resources=r"/api/auth/*")


@auth.route("/register", methods=["POST"])
def register():
    """
    Registers a new user and creates new database fields.
    """
    req = request.get_json(force=True)

    if all(k in req for k in ["first_name", "last_name", "email", "password"]):
        first_name = req["first_name"]
        last_name = req["last_name"]
        email = req["email"].lower()
        password = req["password"]

        exist_email = UserModel.objects.filter(email=email).first()

        if exist_email:
            res = {"status": "Error", "message": "Email address is already taken"}
            return jsonify(res), 409, default_headers

        else:
            password_hash = bcrypt.generate_password_hash(password).decode("utf-8")
            new_user = UserModel(
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=password_hash
            )
            new_user.save()

            res = {"status": "OK", "message": "User has successfully registered"}
            return jsonify(res), 200, default_headers
    else:
        res = {"status": "Error", "message": "Please provide all required fields"}
        return jsonify(res), 400, default_headers


@auth.route("/login", methods=["POST"])
def login():
    """
    Logs the user in and saves login info (browser info, location, etc.).
    """
    req = request.get_json(force=True)
    email = req["email"].lower()
    password = req["password"]
    login_info = req["login_info"]
    current_user = UserModel.objects.filter(email=email).first()

    if current_user and bcrypt.check_password_hash(current_user.password, password):
        info = get_user_info(current_user)
        now = datetime.now(timezone.utc).astimezone()

        # Add new login info
        new_login = LoginInfoModel(
            user_id=str(current_user.id),
            user_agent=login_info.get("user_agent", ""),
            language=login_info.get("language", ""),
            ip=login_info.get("ip", ""),
            isp=login_info.get("isp", ""),
            connection_type=login_info.get("connection_type", ""),
            city=login_info.get("city", ""),
            region=login_info.get("region", ""),
            country=login_info.get("country", ""),
            time_zone=login_info.get("time_zone", ""),
            login_timestamp=now
        )
        new_login.save()

        # Update user info
        current_user.nr_logins = current_user.nr_logins + 1
        current_user.last_login = now
        current_user.save()

        # Create JWT
        access_token = create_access_token(identity=email)
        refresh_token = create_refresh_token(identity=email)
        res = jsonify({
            "status": "OK",
            "message": "User logged in",
            "user_info": info,
            "completed_questions": current_user.completed_questions,
            "rated_questions": current_user.rated_questions
        })
        set_access_cookies(res, access_token)
        set_refresh_cookies(res, refresh_token)
        return res, 200, default_headers

    else:
        res = {"status": "Error", "message": "Wrong login credentials"}
        return jsonify(res), 401, default_headers


@auth.route("/refresh", methods=["POST"])
@jwt_refresh_token_required
def refresh():
    """
    Refreshes a valid JWT.
    """
    email = get_jwt_identity()
    access_token = create_access_token(identity=email)
    res = jsonify({"status": "OK", "message": "JWT refreshed"})
    set_access_cookies(res, access_token)
    return res, 200, default_headers


@auth.route("/logout", methods=["GET"])
def logout():
    """
    Logs the user out.
    """
    res = jsonify({"status": "OK", "message": "User logged out"})
    unset_jwt_cookies(res)
    return res, 200, default_headers
