from flask import request, Blueprint, jsonify
from flask_cors import CORS
from flask_jwt_extended import jwt_required, get_jwt_identity

from src.constants import default_headers
from src.helpers import calculate_new_rating
from src.db.models import User as UserModel, Question as QuestionModel

questions = Blueprint("questions", __name__)
CORS(questions, resources=r"/api/questions/*")


@questions.route("/rate", methods=["POST"])
@jwt_required
def rate_question():
    """
    Add a user rating to a question.
    """
    req = request.get_json(force=True)
    question_id = req["id"]
    rating = req["rating"]

    if question_id and rating:
        email = get_jwt_identity()
        user = UserModel.objects.filter(email=email).first()

        rated_questions = user.rated_questions

        if question_id not in rated_questions:
            rated_questions.append(question_id)

            # Update user
            user.rated_questions = rated_questions
            user.save()

            # Add or update question
            question = QuestionModel.objects.filter(question_id=question_id).first()

            if question is None:
                # Add question with rating to database
                question = QuestionModel(
                    question_id=question_id,
                    nr_ratings=1,
                    overall_rating=float(rating)
                )
                question.save()

            else:
                new_total_ratings, new_overall_rating = calculate_new_rating(rating,
                    question.nr_ratings, question.overall_rating)

                # Update question in database
                question.nr_ratings = new_total_ratings
                question.overall_rating = new_overall_rating
                question.save()

            res = jsonify({
                "status": "OK",
                "rated_questions": rated_questions,
                "rating": {
                  "nr_ratings": question.nr_ratings,
                  "overall_rating": round(question.overall_rating, 3)
                }
            })
            return res, 200, default_headers

        else:
            res = {"status": "Error", "message": "User has already rated this question"}
            return jsonify(res), 400, default_headers

    else:
        res = {"status": "Error", "message": "Bad request"}
        return jsonify(res), 400, default_headers


@questions.route("/complete", methods=["POST"])
@jwt_required
def toggle_complete():
    """
    Toggle a question as completed or not completed.
    """
    req = request.get_json(force=True)

    try:
        email = get_jwt_identity()
        user = UserModel.objects.filter(email=email).first()

        new_id = req["id"]
        current_completed = user.completed_questions

        if new_id in current_completed:
            current_completed.remove(new_id)
            new_completed = current_completed
        else:
            new_completed = []

            if len(current_completed) > 0:
                new_completed = [i for i in current_completed]

            new_completed.append(new_id)

        user.completed_questions = new_completed
        user.save()

        res = jsonify({"status": "OK", "completed_questions": new_completed})
        return res, 200, default_headers

    except:
        res = {"status": "Error", "message": "Bad request"}
        return jsonify(res), 400, default_headers


@questions.route("/completed", methods=["GET"])
@jwt_required
def get_completed():
    """
    Get list of completed questions.
    """
    req = request.get_json(force=True)

    try:
        email = get_jwt_identity()
        user = UserModel.objects.filter(email=email).first()

        completed_questions = user.completed_questions
        res = jsonify({"status": "OK", "completed_questions": completed_questions})
        return res, 200, default_headers

    except:
        res = {"status": "Error", "message": "Bad request"}
        return jsonify(res), 400, default_headers


@questions.route("/all", methods=["GET"])
@jwt_required
def get_all_questions():
    """
    Get all questions with a rating.
    """
    all_questions = QuestionModel.objects.all()

    questions = []
    for q in all_questions:
        questions.append({
            "id": str(q.id),
            "question_id": q.question_id,
            "nr_ratings": q.nr_ratings,
            "overall_rating": round(q.overall_rating, 3)
        })

    res = jsonify({"status": "OK", "questions": questions})
    return res, 200, default_headers


@questions.route("/ratings", methods=["GET"])
@jwt_required
def get_ratings():
    """
    Get all question ratings.
    Return a dictionary with question_id as key.
    """
    all_questions = QuestionModel.objects.all()

    data = {}
    for q in all_questions:
        key = q.question_id
        data[key] = {
            "nr_ratings": q.nr_ratings,
            "overall_rating": round(q.overall_rating, 3)
        }

    res = jsonify({"status": "OK", "data": data})
    return res, 200, default_headers
