import logging

from flask import Flask
from flask_cors import CORS
from flask_mongoengine import MongoEngine
from flask_jwt_extended import JWTManager

from src import flask_config
from src.env_config import FLASK_ENV

from src.routes.auth import auth
from src.routes.users import users
from src.routes.questions import questions


def create_app():
    app = Flask(__name__)
    app.config.from_object(flask_config)

    if FLASK_ENV == "development":
        CORS(app, supports_credentials=True)

    logging.basicConfig(format="%(asctime)s %(levelname)s [%(module)s %(lineno)d] %(message)s",
                        level=app.config["LOG_LEVEL"])

    db = MongoEngine(app)
    jwt = JWTManager(app)

    app.register_blueprint(auth, url_prefix="/api/auth")
    app.register_blueprint(users, url_prefix="/api/users")
    app.register_blueprint(questions, url_prefix="/api/questions")

    return app
