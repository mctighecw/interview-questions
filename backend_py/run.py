from src.app import create_app
from src.env_config import APP_PORT, FLASK_ENV

if __name__ == "__main__":
    app = create_app()
    development_mode = FLASK_ENV == "development"

    print(f"--- Starting web server on port {APP_PORT}")

    app.run(port=APP_PORT, host="0.0.0.0", debug=development_mode)
