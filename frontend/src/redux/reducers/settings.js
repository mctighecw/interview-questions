import * as types from 'Redux/actions/actionTypes';

const initialState = {
  showAuth: '',
  showCookieModal: false,
  userInfo: {
    first_name: '',
    last_name: '',
    email: '',
    registered_at: '',
  },
  sidebarOpen: false,
  mode: 'dark',
};

const settings = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.SHOW_AUTH_MODAL: {
      return {
        ...state,
        showAuth: action.data,
      };
    }
    case types.TOGGLE_COOKIE_MODAL: {
      return {
        ...state,
        showCookieModal: action.data,
      };
    }
    case types.SET_USER_INFO: {
      return {
        ...state,
        userInfo: { ...action.payload },
      };
    }
    case types.CLEAR_USER_INFO: {
      return {
        ...state,
        userInfo: initialState.userInfo,
      };
    }
    case types.TOGGLE_SIDEBAR: {
      return {
        ...state,
        sidebarOpen: !state.sidebarOpen,
      };
    }
    case types.TOGGLE_DARK_MODE: {
      const mode = state.mode === 'light' ? 'dark' : 'light';

      return {
        ...state,
        mode,
      };
    }
    default:
      return state;
  }
};

export default settings;
