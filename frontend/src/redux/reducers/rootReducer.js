import { combineReducers } from 'redux';
import questions from './questions';
import settings from './settings';

const rootReducer = combineReducers({
  questions,
  settings,
});

export default rootReducer;
