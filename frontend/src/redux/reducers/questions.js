import * as types from 'Redux/actions/actionTypes';
import { getData } from 'Utils/data';

const initialState = {
  category: 'All categories',
  difficulty: 'All difficulties',
  showCompleted: 'All questions',
  currentQuestion: 0,
  completedQuestions: [],
  ratedQuestions: [],
  ratings: {},
};

const questions = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.CHANGE_QUESTION_VALUE: {
      return {
        ...state,
        currentQuestion: 0,
        [action.field]: action.value,
      };
    }
    case types.CHANGE_QUESTION_INDEX: {
      const { showCompleted, completedQuestions, category, difficulty } = state;
      let { filteredData } = getData(category, difficulty, showCompleted, completedQuestions);
      let value = 0;

      if (action.direction === 'next') {
        value = state.currentQuestion + 1;
        if (value === filteredData.length) value = 0;
      } else {
        value = state.currentQuestion - 1;
        if (value + 1 === 0) value = filteredData.length - 1;
      }

      return {
        ...state,
        currentQuestion: value,
      };
    }
    case types.UPDATE_COMPLETED_QUESTIONS: {
      return {
        ...state,
        completedQuestions: action.data,
      };
    }
    case types.UPDATE_RATED_QUESTIONS: {
      return {
        ...state,
        ratedQuestions: action.data,
      };
    }
    case types.UPDATE_RATED: {
      const ratedQuestions = [...state.ratedQuestions];
      ratedQuestions.push(action.data);

      return {
        ...state,
        ratedQuestions,
      };
    }
    case types.SET_RATINGS: {
      return {
        ...state,
        ratings: action.data,
      };
    }
    case types.UPDATE_RATING: {
      let ratings = { ...state.ratings };
      const { id, rating } = action.data;
      ratings[id] = rating;

      return {
        ...state,
        ratings,
      };
    }
    case types.CLEAR_QUESTIONS_INFO: {
      return initialState;
    }
    default:
      return state;
  }
};

export default questions;
