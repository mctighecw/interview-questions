import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import logger from 'redux-logger';
import rootReducer from 'Redux/reducers/rootReducer';

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const middlewares = applyMiddleware(logger);

const store =
  process.env.NODE_ENV === 'development'
    ? createStore(persistedReducer, middlewares)
    : createStore(persistedReducer);

const persistor = persistStore(store);

export { store, persistor };
