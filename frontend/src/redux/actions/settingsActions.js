import * as types from './actionTypes';

// User
export const setUserInfo = (payload) => ({
  type: types.SET_USER_INFO,
  payload,
});

export const clearUserInfo = () => ({
  type: types.CLEAR_USER_INFO,
});

// Page
export const toggleSidebar = () => ({
  type: types.TOGGLE_SIDEBAR,
});

export const toggleDarkMode = () => ({
  type: types.TOGGLE_DARK_MODE,
});

// Auth
export const showAuthModal = (data) => ({
  type: types.SHOW_AUTH_MODAL,
  data,
});

export const toggleCookieModal = (data) => ({
  type: types.TOGGLE_COOKIE_MODAL,
  data,
});
