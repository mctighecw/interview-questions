import * as types from './actionTypes';

export const changeQuestionValue = (field, value) => ({
  type: types.CHANGE_QUESTION_VALUE,
  field,
  value,
});

export const changeQuestion = (direction) => ({
  type: types.CHANGE_QUESTION_INDEX,
  direction,
});

export const updateCompletedQuestions = (data) => ({
  type: types.UPDATE_COMPLETED_QUESTIONS,
  data,
});

export const updateRatedQuestions = (data) => ({
  type: types.UPDATE_RATED_QUESTIONS,
  data,
});

export const updateRated = (data) => ({
  type: types.UPDATE_RATED,
  data,
});

export const setRatings = (data) => ({
  type: types.SET_RATINGS,
  data,
});

export const updateRating = (data) => ({
  type: types.UPDATE_RATING,
  data,
});

export const clearQuestionsInfo = () => ({
  type: types.CLEAR_QUESTIONS_INFO,
});
