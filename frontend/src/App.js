import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from 'Redux/store';
import Main from 'Containers/Main';
import './styles/global.css';

const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Router>
        <Switch>
          <Route exact path="/" component={Main} />
          <Route render={() => <Redirect to="/" />} />
        </Switch>
      </Router>
    </PersistGate>
  </Provider>
);

export default App;
