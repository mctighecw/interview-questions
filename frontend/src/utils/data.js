import { difficultyKeys } from 'Constants/constants';
import { categoryOptions, difficultyOptions, completedOptions } from 'Constants/options';
import data from '../../interview-questions-data/questions';

export const getData = (category, difficulty, showCompleted, completedQuestions) => {
  let filteredData = [];

  let allCategories = [];
  Object.keys(data).forEach((key) => {
    allCategories = [...allCategories, ...data[key]];
  });

  // Filter by category
  if (category === categoryOptions[0]) {
    filteredData = allCategories;
  } else {
    filteredData = data[category];
  }

  // Filter by diffculty
  if (difficulty !== difficultyOptions[0]) {
    filteredData = filteredData.filter((item) => item.difficulty === difficultyKeys[difficulty]);
  }

  // Filter (un)completed
  if (showCompleted === completedOptions[1]) {
    filteredData = filteredData.filter((item) => !completedQuestions.includes(item.id));
  } else if (showCompleted === completedOptions[2]) {
    filteredData = filteredData.filter((item) => completedQuestions.includes(item.id));
  }
  return { filteredData, dataLength: allCategories.length };
};
