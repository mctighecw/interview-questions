import dayjs from 'dayjs';

export const getErrorMessage = (err) => {
  let result = 'An error has occurred';

  if (!!err?.response?.data?.message) {
    result = err.response.data.message;
  }
  return result;
};

export const formatDate = (datestring, short = true) => {
  const format = short ? 'DD-MM-YY' : 'DD MMM YYYY';
  const res = dayjs(datestring).format(format);
  return res;
};
