import axios from 'axios';
import { store } from 'Redux/store';
import { toggleCookieModal } from 'Redux/actions/settingsActions';

/*
  Use the 'application/x-www-form-urlencoded' content-type header only for Flask backend in development.
  The reason is that Flask CORS does not add the response header 'Access-Control-Allow-Credentials',
  which is necessary for the JWT cookie authentication, when the preflight OPTIONS request is made
  (that goes with a request using the content-type header 'application/json'). (The latter works fine in
  production, with the same origin.) Using 'application/x-www-form-urlencoded' makes a simple request
  and thus avoids preflight OPTIONS.
*/
const flaskDevHeader = process.env.NODE_ENV === 'development' && process.env.BACKEND_PORT === 7500;
const headers = flaskDevHeader
  ? { 'Content-Type': 'application/x-www-form-urlencoded' }
  : { 'Content-Type': 'application/json' };

const api = axios.create({
  timeout: 10000,
  withCredentials: true,
  headers,
});

api.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error?.response?.status === 401 && error.response?.data?.message !== 'Wrong login credentials') {
      // Show modal if cookie expires
      store.dispatch(toggleCookieModal(true));
    }
    return Promise.reject(error);
  }
);

export const get = (url) => {
  return api.get(url);
};

export const post = (url, data) => {
  return api.post(url, data);
};
