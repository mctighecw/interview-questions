const getPrefix = () => {
  const port = process.env.BACKEND_PORT;
  const prefix = process.env.NODE_ENV === 'development' ? `http://localhost:${port}` : '';
  return prefix;
};

const getUrl = (category, type) => {
  const prefix = getPrefix();
  return `${prefix}/api/${category}/${type}`;
};

export const getAuthUrl = (type) => getUrl('auth', type);
export const getQuestionsUrl = (type) => getUrl('questions', type);
