export const difficultyValues = {
  '1': 'Easy',
  '2': 'Medium',
  '3': 'Challenging',
};

export const difficultyKeys = {
  Easy: 1,
  Medium: 2,
  Challenging: 3,
};
