export const categoryOptions = ['All categories', 'General', 'HTML', 'JavaScript', 'React', 'Styling', 'Web'];

export const difficultyOptions = ['All difficulties', 'Challenging', 'Medium', 'Easy'];

export const completedOptions = ['All questions', 'Only uncompleted', 'Only completed'];
