import { connect } from 'react-redux';
import { showAuthModal } from 'Redux/actions/settingsActions';
import Container from 'Components/Auth/Container/Container';

const mapStateToProps = (state) => ({
  settings: state.settings,
});

const mapDispatchToProps = (dispatch) => ({
  handleShowAuthModal: (data) => dispatch(showAuthModal(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);
