import { connect } from 'react-redux';
import { get } from 'Utils/requests';
import { getQuestionsUrl } from 'Utils/network';
import { setUserInfo, showAuthModal } from 'Redux/actions/settingsActions';
import { updateCompletedQuestions, updateRatedQuestions, setRatings } from 'Redux/actions/questionsActions';
import Login from 'Components/Auth/Login/Login';

const mapStateToProps = (state) => ({
  settings: state.settings,
});

const mapDispatchToProps = (dispatch) => ({
  handleSetUserInfo: (data) => {
    dispatch(setUserInfo(data));
  },
  handleSetQuestionData: (completedData, ratingsData) => {
    dispatch(updateCompletedQuestions(completedData));
    dispatch(updateRatedQuestions(ratingsData));
  },
  handleSetRatings: () => {
    const url = getQuestionsUrl('ratings');

    get(url)
      .then((res) => {
        if (!!res?.data?.data) {
          dispatch(setRatings(res.data.data));
        }
      })
      .catch((err) => {
        console.log(err);
      });
  },
  handleShowAuthModal: (data) => dispatch(showAuthModal(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
