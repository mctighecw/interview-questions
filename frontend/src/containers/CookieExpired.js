import { connect } from 'react-redux';
import { toggleCookieModal, showAuthModal } from 'Redux/actions/settingsActions';
import CookieExpired from 'Components/CookieExpired/CookieExpired';

const mapStateToProps = (state) => ({
  settings: state.settings,
});

const mapDispatchToProps = (dispatch) => ({
  handleToggleCookieModal: (data) => dispatch(toggleCookieModal(data)),
  handleShowAuthModal: (data) => dispatch(showAuthModal(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CookieExpired);
