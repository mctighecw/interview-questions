import { connect } from 'react-redux';
import { toggleSidebar, showAuthModal, clearUserInfo } from 'Redux/actions/settingsActions';
import { clearQuestionsInfo } from 'Redux/actions/questionsActions';
import { get } from 'Utils/requests';
import { getAuthUrl } from 'Utils/network';
import TopMenu from 'Components/TopMenu/TopMenu';

const mapStateToProps = (state) => ({
  settings: state.settings,
});

const mapDispatchToProps = (dispatch) => ({
  handleToggleSidebar: () => dispatch(toggleSidebar()),
  handleShowAuthModal: (data) => dispatch(showAuthModal(data)),
  handleLogout: () => {
    const url = getAuthUrl('logout');

    get(url)
      .then((res) => {
        dispatch(clearUserInfo());
        dispatch(clearQuestionsInfo());
      })
      .catch((err) => {
        console.log(err);
      });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TopMenu);
