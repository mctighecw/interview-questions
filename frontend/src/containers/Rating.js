import { connect } from 'react-redux';
import { post } from 'Utils/requests';
import { getQuestionsUrl } from 'Utils/network';
import { updateRating, updateRated } from 'Redux/actions/questionsActions';
import Rating from 'Components/Rating/Rating';

const mapStateToProps = (state) => ({
  questions: state.questions,
});

const mapDispatchToProps = (dispatch) => ({
  handleRateQuestion: (id, rating) => {
    const url = getQuestionsUrl('rate');
    const data = { id, rating };

    post(url, data)
      .then((res) => {
        if (res.data.status === 'OK') {
          const newRating = { id, rating: res.data.rating };
          dispatch(updateRating(newRating));
          dispatch(updateRated(id));
        }
      })
      .catch((err) => {
        console.log(err);
      });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Rating);
