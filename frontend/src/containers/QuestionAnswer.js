import { connect } from 'react-redux';
import { post } from 'Utils/requests';
import { getQuestionsUrl } from 'Utils/network';
import { updateCompletedQuestions } from 'Redux/actions/questionsActions';
import QuestionAnswer from 'Components/QuestionAnswer/QuestionAnswer';

const mapStateToProps = (state) => ({
  settings: state.settings,
  questions: state.questions,
});

const mapDispatchToProps = (dispatch) => ({
  handleToggleComplete: (id) => {
    const url = getQuestionsUrl('complete');
    const data = { id };

    post(url, data)
      .then((res) => {
        const { data } = res;
        dispatch(updateCompletedQuestions(data.completed_questions));
      })
      .catch((err) => {
        console.log(err);
      });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(QuestionAnswer);
