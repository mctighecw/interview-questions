import { connect } from 'react-redux';
import { changeQuestionValue } from 'Redux/actions/questionsActions';
import OptionsRow from 'Components/OptionsRow/OptionsRow';

const mapStateToProps = (state) => ({
  questions: state.questions,
});

const mapDispatchToProps = (dispatch) => ({
  handleChangeValue: (field, value) => {
    dispatch(changeQuestionValue(field, value));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(OptionsRow);
