import { connect } from 'react-redux';
import Main from 'Components/Main/Main';

const mapStateToProps = (state) => ({
  settings: state.settings,
  questions: state.questions,
});

export default connect(mapStateToProps, null)(Main);
