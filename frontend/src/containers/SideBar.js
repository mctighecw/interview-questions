import { connect } from 'react-redux';
import { toggleDarkMode, toggleSidebar } from 'Redux/actions/settingsActions';
import SideBar from 'Components/SideBar/SideBar';

const mapStateToProps = (state) => ({
  settings: state.settings,
});

const mapDispatchToProps = (dispatch) => ({
  handleToggleMode: () => dispatch(toggleDarkMode()),
  handleToggleSidebar: () => dispatch(toggleSidebar()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
