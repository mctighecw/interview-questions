import { connect } from 'react-redux';
import { changeQuestion } from 'Redux/actions/questionsActions';
import NavigationRow from 'Components/NavigationRow/NavigationRow';

const mapDispatchToProps = (dispatch) => ({
  handleChangeQuestion: (direction) => {
    dispatch(changeQuestion(direction));
  },
});

export default connect(null, mapDispatchToProps)(NavigationRow);
