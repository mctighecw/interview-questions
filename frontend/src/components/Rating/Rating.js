import React, { useState, useEffect } from 'react';
import StarRatings from 'react-star-ratings';
import HoverBox from './HoverBox/HoverBox';

import { WHITE, LIGHT_GREY, PEPPERMINT } from 'Stylesheet/constants';
import './styles.less';

const Rating = (props) => {
  const { questionId, questions, handleRateQuestion } = props;
  const { ratings } = questions;
  const [questionRating, setQuestionRating] = useState(0);
  const [alreadyRated, setAlreadyRated] = useState(true);
  const [showInfo, setShowInfo] = useState(false);

  const onChangeRating = (value) => {
    handleRateQuestion(questionId, value);
  };

  useEffect(() => {
    const rating = !!ratings[questionId] ? ratings[questionId].overall_rating : 0;
    const rated = questions.ratedQuestions.includes(questionId);
    setQuestionRating(rating);
    setAlreadyRated(rated);
  }, [questionId, questions]);

  // Hide .star-ratings title
  const el = document.getElementsByClassName('star-ratings')[0];
  if (el) el.title = '';

  return (
    <div
      onMouseOver={(e) => setShowInfo(true)}
      onMouseLeave={(e) => setShowInfo(false)}
      styleName="container"
    >
      <StarRatings
        rating={questionRating}
        numOfStars={3}
        starEmptyColor={LIGHT_GREY}
        starRatedColor={PEPPERMINT}
        starSelectingHoverColor={WHITE}
        starWidthAndHeight="18px"
        starSpacing="2px"
        isSelectable={!alreadyRated}
        changeRating={onChangeRating}
      />

      {showInfo && !!ratings[questionId] && (
        <HoverBox info={ratings[questionId]} alreadyRated={alreadyRated} />
      )}
    </div>
  );
};

export default Rating;
