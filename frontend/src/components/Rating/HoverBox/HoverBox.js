import React, { Fragment } from 'react';
import './styles.less';

const HoverBox = (props) => {
  const { info, alreadyRated } = props;
  const { nr_ratings, overall_rating } = info;

  const renderFields = () => (
    <Fragment>
      <div styleName="line">Summary:</div>
      <div styleName="line">
        <span>Overall rating:</span>
        {overall_rating}
      </div>
      <div styleName="line">
        <span>Total ratings:</span>
        {nr_ratings}
      </div>
      {alreadyRated && <div styleName="line last">You've rated this question</div>}
    </Fragment>
  );

  return <div styleName="container">{renderFields()}</div>;
};

export default HoverBox;
