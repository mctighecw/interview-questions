import React from 'react';

import SideBar from 'Containers/SideBar';
import TopMenu from 'Containers/TopMenu';
import OptionsRow from 'Containers/OptionsRow';
import QuestionAnswer from 'Containers/QuestionAnswer';
import NavigationRow from 'Containers/NavigationRow';

import AuthContainer from 'Containers/Auth/Container';
import CookieExpired from 'Containers/CookieExpired';

import { getData } from 'Utils/data';
import './styles.less';

const Main = (props) => {
  const { settings, questions } = props;
  const { mode, userInfo, showAuth, showCookieModal } = settings;
  const { showCompleted, completedQuestions, category, difficulty } = questions;
  const { filteredData, dataLength } = getData(category, difficulty, showCompleted, completedQuestions);
  const loggedIn = userInfo.email !== '';

  return (
    <div styleName={`container ${mode}`}>
      <SideBar />

      <div styleName="main">
        <TopMenu />
        <div styleName="content">
          <div styleName={`description margin-bottom ${mode}`}>
            Choose your filtering criteria below, then start answering questions.
          </div>
          {!loggedIn && (
            <div styleName={`description margin-bottom ${mode}`}>
              To mark questions as completed, please log in.
              <br />
              If you don't have an account, then go ahead and sign up!
            </div>
          )}

          <div styleName={`text ${mode}`}>Total number of questions: {dataLength}</div>
          {loggedIn && (
            <div styleName={`text ${mode}`}>Completed questions: {questions.completedQuestions.length}</div>
          )}
          <div styleName={`text margin-bottom ${mode}`}>
            Questions matching criteria: {filteredData.length}
          </div>

          <OptionsRow />
          <QuestionAnswer filteredData={filteredData} />
          <NavigationRow dataLength={filteredData.length} />
        </div>
      </div>

      {showAuth !== '' && <AuthContainer />}
      {showCookieModal && <CookieExpired />}
    </div>
  );
};

export default Main;
