import React, { useState, useEffect } from 'react';
import FullButton from 'Components/shared/buttons/FullButton/FullButton';
import OutlineButton from 'Components/shared/buttons/OutlineButton/OutlineButton';
import styles from './styles.less';
import './styles.css';

const Answer = ({ answer }) => {
  const [showAnswer, setShowAnswer] = useState(false);

  useEffect(() => {
    setShowAnswer(false);
  }, [answer]);

  return (
    <div styleName="styles.container">
      {showAnswer ? (
        <div styleName="styles.content">
          <div styleName="styles.title">Answer</div>
          <div styleName="styles.answer" dangerouslySetInnerHTML={{ __html: answer }} />

          <OutlineButton label="Close" onClickMethod={() => setShowAnswer(false)} />
        </div>
      ) : (
        <FullButton label="Show answer" onClickMethod={() => setShowAnswer(true)} />
      )}
    </div>
  );
};

export default Answer;
