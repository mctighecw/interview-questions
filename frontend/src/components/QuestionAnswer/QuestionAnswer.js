import React, { Fragment } from 'react';
import Rating from 'Containers/Rating';
import CheckBox from 'Components/shared/inputs/CheckBox/CheckBox';
import Answer from 'Components/Answer/Answer';

import { difficultyValues } from 'Constants/constants';
import './styles.less';

const QuestionAnswer = (props) => {
  const { filteredData, settings, questions, handleToggleComplete } = props;
  const { currentQuestion, completedQuestions } = questions;

  if (!!filteredData[currentQuestion]) {
    const thisQuestion = filteredData[currentQuestion];
    const { id, question, answer, category, difficulty } = thisQuestion;
    const loggedIn = settings.userInfo.email !== '';
    const completed = completedQuestions.includes(id);

    const handleToggleCheck = () => {
      handleToggleComplete(id);
    };

    return (
      <div styleName="container">
        <div styleName="content">
          <div styleName="title">Question</div>
          <div styleName="info">
            <div styleName="field">CATEGORY:</div>
            <div>{category}</div>
          </div>
          <div styleName="info">
            <div styleName="field">DIFFICULTY:</div>
            <div>{difficultyValues[difficulty]}</div>
          </div>
          {loggedIn && (
            <Fragment>
              <div styleName="info">
                <div styleName="field middle">RATING:</div>
                <Rating questionId={id} />
              </div>
              <div styleName="info">
                <div styleName="field middle">COMPLETED:</div>
                <CheckBox checked={completed} onChangeMethod={handleToggleCheck} />
              </div>
            </Fragment>
          )}

          <div styleName="text">{question}</div>

          <Answer answer={answer} />
        </div>
      </div>
    );
  } else {
    return (
      <div styleName="container">
        <div styleName="content">
          <div styleName="text">No questions for the above criteria</div>
        </div>
      </div>
    );
  }
};

export default QuestionAnswer;
