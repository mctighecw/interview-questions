import React from 'react';
import Modal from 'Components/shared/Modal/Modal';
import Login from 'Containers/Auth/Login';
import SignUp from '../SignUp/SignUp';

const Container = (props) => {
  const { settings, handleShowAuthModal } = props;
  const { mode, showAuth } = settings;

  return (
    <Modal mode={mode} onClickCancel={() => handleShowAuthModal('')}>
      {showAuth === 'login' ? <Login /> : <SignUp mode={mode} handleShowAuthModal={handleShowAuthModal} />}
    </Modal>
  );
};

export default Container;
