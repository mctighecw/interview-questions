import React from 'react';
import { get, post } from 'Utils/requests';
import { getAuthUrl } from 'Utils/network';
import { getErrorMessage } from 'Utils/functions';

import AuthInput from 'Components/shared/inputs/AuthInput/AuthInput';
import FullButton from 'Components/shared/buttons/FullButton/FullButton';
import '../styles.less';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: 'john@smith.com',
      password: 'john1',
      loginInfo: {},
      error: '',
    };
  }

  handleFieldChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value, error: '' });
  };

  handleInputKeyPress = (event) => {
    if (event.which === 13 || event.keyCode === 13) {
      this.handleLoginUser();
    }
  };

  handleLoginUser = () => {
    const { email, password, loginInfo } = this.state;
    const { handleSetUserInfo, handleSetQuestionData, handleSetRatings, handleShowAuthModal } = this.props;

    this.setState({ error: '' });

    if (email !== '' && password !== '') {
      const url = getAuthUrl('login');
      const payload = { email, password, login_info: loginInfo };

      post(url, payload)
        .then((res) => {
          const { data } = res;
          if (data.status === 'OK') {
            const { user_info, completed_questions, rated_questions } = data;
            handleSetUserInfo(user_info);
            handleSetQuestionData(completed_questions, rated_questions);
            handleSetRatings();
            handleShowAuthModal('');
          } else {
            this.setState({ error: data.message });
          }
        })
        .catch((err) => {
          console.log(err);
          const message = getErrorMessage(err);
          this.setState({ error: message });
        });
    } else {
      this.setState({ error: 'Please fill out both fields' });
    }
  };

  handleGetUserInfo = () => {
    return new Promise((resolve, reject) => {
      const { userAgent, language } = navigator;

      this.setState(
        {
          loginInfo: {
            ...this.state.loginInfo,
            user_agent: userAgent,
            language,
          },
        },
        () => {
          resolve();
        }
      );
    });
  };

  handeleGetUserIp = () => {
    return new Promise((resolve, reject) => {
      const prod = process.env.NODE_ENV === 'production';

      if (prod) {
        const url = 'https://json.geoiplookup.io';

        fetch(url)
          .then((res) => res.json())
          .then((data) => {
            const { ip, isp, connection_type, city, region, country_name, timezone_name } = data;

            this.setState({
              loginInfo: {
                ...this.state.loginInfo,
                ip,
                isp,
                connection_type,
                city,
                region,
                country: country_name,
                time_zone: timezone_name,
              },
            });
            resolve();
          })
          .catch((err) => {
            console.log(err);
            resolve();
          });
      }
      resolve();
    });
  };

  getAllInfo = async () => {
    await this.handleGetUserInfo();
    await this.handeleGetUserIp();
  };

  handleAuthLink = () => {
    this.clearMessages();
    this.props.handleShowAuthModal('signup');
  };

  clearMessages = () => {
    this.setState({ error: '' });
  };

  componentDidMount() {
    this.getAllInfo();
  }

  render() {
    const { mode } = this.props.settings;
    const { email, password, error } = this.state;

    return (
      <div styleName={`container ${mode}`}>
        <div styleName={`heading ${mode}`}>Login</div>

        <AuthInput
          type="text"
          value={email}
          name="email"
          placeholder="Email"
          mode={mode}
          onChangeMethod={this.handleFieldChange}
        />

        <AuthInput
          type="password"
          value={password}
          name="password"
          placeholder="Password"
          mode={mode}
          onKeyPressMethod={this.handleInputKeyPress}
          onChangeMethod={this.handleFieldChange}
        />

        <div styleName="error">{error}</div>

        <FullButton label="Log in" onClickMethod={this.handleLoginUser} />

        <div styleName={`link ${mode}`} onClick={this.handleAuthLink}>
          Go to sign up
        </div>
      </div>
    );
  }
}

export default Login;
