import React from 'react';
import { post } from 'Utils/requests';
import { getAuthUrl } from 'Utils/network';
import { getErrorMessage } from 'Utils/functions';

import AuthInput from 'Components/shared/inputs/AuthInput/AuthInput';
import FullButton from 'Components/shared/buttons/FullButton/FullButton';

import '../styles.less';

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      passwordReview: '',
      error: '',
    };
  }

  handleFieldChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value, error: '' });
  };

  handleInputKeyPress = (event) => {
    if (event.which === 13 || event.keyCode === 13) {
      this.handleSignUpUser();
    }
  };

  handleSignUpUser = () => {
    const { first_name, last_name, email, password, passwordReview } = this.state;
    const { handleShowAuthModal } = this.props;

    this.clearMessages();

    if (first_name !== '' && last_name !== '' && email !== '' && password !== '') {
      if (password !== passwordReview) {
        this.setState({ error: 'Your passwords do not match' });
      } else {
        const url = getAuthUrl('register');
        const data = { first_name, last_name, email, password };

        post(url, data)
          .then((res) => {
            handleShowAuthModal('login');
          })
          .catch((err) => {
            console.log(err);
            const message = getErrorMessage(err);
            this.setState({ error: message });
          });
      }
    } else {
      this.setState({ error: 'Please fill out all fields' });
    }
  };

  handleAuthLink = () => {
    this.clearMessages();
    this.props.handleShowAuthModal('login');
  };

  clearMessages = () => {
    this.setState({ error: '' });
  };

  render() {
    const { mode } = this.props;
    const { first_name, last_name, email, password, passwordReview, error } = this.state;

    return (
      <div styleName={`container ${mode}`}>
        <div styleName={`heading ${mode}`}>Sign Up</div>

        <AuthInput
          type="text"
          value={first_name}
          name="first_name"
          maxLength={15}
          placeholder="First name"
          mode={mode}
          onChangeMethod={this.handleFieldChange}
        />

        <AuthInput
          type="text"
          value={last_name}
          name="last_name"
          maxLength={15}
          placeholder="Last name"
          mode={mode}
          onChangeMethod={this.handleFieldChange}
        />

        <AuthInput
          type="text"
          value={email}
          name="email"
          maxLength={30}
          placeholder="Email"
          mode={mode}
          onChangeMethod={this.handleFieldChange}
        />

        <AuthInput
          type="password"
          value={password}
          name="password"
          placeholder="Password"
          mode={mode}
          onChangeMethod={this.handleFieldChange}
        />

        <AuthInput
          type="password"
          value={passwordReview}
          name="passwordReview"
          placeholder="Repeat password"
          mode={mode}
          onKeyPress={this.handleInputKeyPress}
          onChangeMethod={this.handleFieldChange}
        />

        <div styleName="error">{error}</div>

        <FullButton label="Sign up" onClickMethod={this.handleSignUpUser} />

        <div styleName={`link ${mode}`} onClick={this.handleAuthLink}>
          Go to login
        </div>
      </div>
    );
  }
}

export default SignUp;
