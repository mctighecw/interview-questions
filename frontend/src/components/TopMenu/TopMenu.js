import React from 'react';
import BurgerMenuLight from 'Assets/light/burger-menu-icon-light.svg';
import BurgerMenuDark from 'Assets/dark/burger-menu-icon-dark.svg';
import CloseIconLight from 'Assets/light/close-icon-light.svg';
import CloseIconDark from 'Assets/dark/close-icon-dark.svg';
import LogoutIconLight from 'Assets/light/logout-icon-light.svg';
import LogoutIconDark from 'Assets/dark/logout-icon-dark.svg';
import './styles.less';

const TopMenu = (props) => {
  const { settings, handleToggleSidebar, handleShowAuthModal, handleLogout } = props;
  const { userInfo, sidebarOpen, mode } = settings;
  const loggedIn = userInfo.email !== '';
  const CloseIcon = mode === 'light' ? CloseIconLight : CloseIconDark;
  const BurgerMenu = mode === 'light' ? BurgerMenuLight : BurgerMenuDark;
  const LogoutIcon = mode === 'light' ? LogoutIconLight : LogoutIconDark;

  const handleLogin = () => {
    handleShowAuthModal('login');
  };

  return (
    <div styleName="container">
      <div styleName="left">
        <img src={sidebarOpen ? CloseIcon : BurgerMenu} onClick={handleToggleSidebar} />
      </div>
      <div styleName="middle">
        <div styleName="title">Interview Questions</div>
      </div>
      <div styleName="right">
        {loggedIn ? (
          <img src={LogoutIcon} onClick={handleLogout} />
        ) : (
          <div styleName={`log-in ${mode}`} onClick={handleLogin}>
            Log in
          </div>
        )}
      </div>
    </div>
  );
};

export default TopMenu;
