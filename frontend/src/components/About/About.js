import React from 'react';
import Modal from 'Components/shared/Modal/Modal';

import reactLogo from 'Assets/react.svg';
import gopherLogo from 'Assets/gopher.svg';
import './styles.less';

const About = (props) => {
  const { settings, handleClose } = props;
  const { mode } = settings;

  return (
    <Modal mode={mode} onClickCancel={handleClose}>
      <div styleName={`about-modal ${mode}`}>
        <div styleName={`text heading ${mode}`}>About</div>

        <div styleName={`text paragraph ${mode}`}>
          This app has a{' '}
          <a href="https://reactjs.org" target="_blank">
            React
          </a>{' '}
          frontend and a{' '}
          <a href="https://golang.org" target="_blank">
            Go
          </a>{' '}
          backend.
        </div>

        <div styleName="logos">
          <img src={reactLogo} title="React" />
          <img src={gopherLogo} title="Go" />
        </div>
      </div>
    </Modal>
  );
};

export default About;
