import React, { useState } from 'react';
import ModeSwitch from 'Components/shared/inputs/ModeSwitch/ModeSwitch';
import About from 'Components/About/About';
import Footer from 'Components/Footer/Footer';
import { formatDate } from 'Utils/functions';

import CloseIconLight from 'Assets/light/close-icon-light.svg';
import CloseIconDark from 'Assets/dark/close-icon-dark.svg';
import './styles.less';

const SideBar = (props) => {
  const [showAbout, setShowAbout] = useState(false);
  const { settings, handleToggleMode, handleToggleSidebar } = props;
  const { userInfo, sidebarOpen, mode } = settings;
  const { first_name, last_name, email, registered_at } = userInfo;
  const loggedIn = email !== '';
  const CloseIcon = mode === 'light' ? CloseIconLight : CloseIconDark;

  if (sidebarOpen) {
    return (
      <div styleName={`side-bar ${mode}`}>
        <div styleName="content">
          <div styleName="box">
            <div styleName="top">
              <div styleName={`title ${mode}`}>Settings</div>
              <img src={CloseIcon} onClick={handleToggleSidebar} />
            </div>
            <ModeSwitch mode={mode} handleToggleMode={handleToggleMode} />

            {loggedIn ? (
              <div styleName="info">
                <div styleName={`title ${mode}`}>User Info</div>
                <div styleName={`row ${mode}`}>{`${first_name} ${last_name}`}</div>
                <div styleName={`row ${mode}`}>{email}</div>
                <div styleName={`row ${mode}`}>
                  Member since:
                  <br />
                  {formatDate(registered_at, false)}
                </div>
              </div>
            ) : (
              <div styleName="info">
                <div styleName={`title ${mode}`}>Info</div>
                <div styleName={`row ${mode}`}>
                  Sign up for free and log in to mark questions as completed and to give them ratings.
                </div>
              </div>
            )}

            <div styleName={`title about ${mode}`} onClick={() => setShowAbout(true)}>
              About
            </div>
          </div>
          <div styleName="footer">
            <Footer mode={mode} />
          </div>
        </div>

        {showAbout && <About settings={settings} handleClose={() => setShowAbout(false)} />}
      </div>
    );
  } else {
    return null;
  }
};

export default SideBar;
