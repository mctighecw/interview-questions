import React from 'react';
import DropDown from 'Components/shared/inputs/DropDown/DropDown';
import { categoryOptions, difficultyOptions, completedOptions } from 'Constants/options';
import './styles.less';

const OptionsRow = (props) => {
  const { questions, handleChangeValue } = props;
  const { category, difficulty, showCompleted } = questions;

  const handleChangeField = (field, value) => {
    handleChangeValue(field, value);
  };

  return (
    <div styleName="options-row">
      <DropDown
        size="normal"
        value={category}
        placeholder=""
        options={categoryOptions}
        onChangeMethod={(e) => handleChangeField('category', e.target.value)}
      />
      <div styleName="separator" />
      <DropDown
        size="normal"
        value={difficulty}
        placeholder=""
        options={difficultyOptions}
        onChangeMethod={(e) => handleChangeField('difficulty', e.target.value)}
      />
      <div styleName="separator" />
      <DropDown
        size="normal"
        value={showCompleted}
        placeholder=""
        options={completedOptions}
        onChangeMethod={(e) => handleChangeField('showCompleted', e.target.value)}
      />
    </div>
  );
};

export default OptionsRow;
