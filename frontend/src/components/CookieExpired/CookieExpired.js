import React from 'react';
import Modal from 'Components/shared/Modal/Modal';
import FullButton from 'Components/shared/buttons/FullButton/FullButton';
import './styles.less';

const CookieExpired = (props) => {
  const { settings, handleToggleCookieModal, handleShowAuthModal } = props;
  const { mode } = settings;

  const handleClickOk = () => {
    handleToggleCookieModal(false);
    handleShowAuthModal('login');
  };

  return (
    <Modal mode={mode} onClickCancel={() => handleToggleCookieModal(false)}>
      <div styleName={`container ${mode}`}>
        <div styleName={`text ${mode}`}>
          Your token has expired.
          <br />
          Please log in again.
        </div>
        <FullButton label="OK" onClickMethod={handleClickOk} />
      </div>
    </Modal>
  );
};

export default CookieExpired;
