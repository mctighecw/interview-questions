import React from 'react';
import './styles.less';

const Footer = ({ mode }) => (
  <div styleName={`container ${mode}`}>
    Coded by Hand.
    <br />
    &copy; 2020 Christian McTighe.
  </div>
);

export default Footer;
