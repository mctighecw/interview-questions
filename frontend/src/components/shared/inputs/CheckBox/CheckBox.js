import React from 'react';
import './styles.less';

const CheckBox = (props) => {
  const { checked, onChangeMethod } = props;

  return (
    <div styleName="container">
      <div styleName="content">
        <label styleName="inner">
          <input type="checkbox" checked={checked} onChange={onChangeMethod} />
          <span styleName="checkmark" />
        </label>
      </div>
    </div>
  );
};

export default CheckBox;
