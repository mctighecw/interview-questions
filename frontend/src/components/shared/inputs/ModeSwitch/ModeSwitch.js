import React from 'react';
import './styles.less';

const ModeSwitch = ({ mode, handleToggleMode }) => (
  <div styleName="toggle">
    <input styleName="toggle-input" type="checkbox" checked={mode === 'dark'} onChange={handleToggleMode} />
    <div styleName="toggle-bg" />
    <div styleName="toggle-switch">
      <div styleName="toggle-switch-figure" />
      <div styleName="toggle-switch-figureAlt" />
    </div>
  </div>
);

export default ModeSwitch;
