import React from 'react';
import './styles.less';

const DropDown = ({ size, value, placeholder, options, onChangeMethod }) => {
  const style = `dropdown ${size}`;

  return (
    <div styleName={style}>
      <select value={value} onChange={onChangeMethod}>
        {placeholder && (
          <option key="placeholder" value="">
            {placeholder}
          </option>
        )}
        {options.length > 0 &&
          options.map((item, index) => (
            <option key={index} value={item}>
              {item}
            </option>
          ))}
      </select>
    </div>
  );
};

export default DropDown;
