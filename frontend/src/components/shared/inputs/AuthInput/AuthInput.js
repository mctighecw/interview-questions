import React from 'react';
import './styles.less';

const AuthInput = ({ type, value, name, maxLength, placeholder, mode, onKeyPressMethod, onChangeMethod }) => (
  <div styleName="container">
    <input
      type={type}
      value={value}
      name={name}
      maxLength={maxLength}
      autofill="false"
      placeholder={placeholder}
      onKeyPress={onKeyPressMethod}
      onChange={onChangeMethod}
      styleName={`input ${mode}`}
    />
  </div>
);

export default AuthInput;
