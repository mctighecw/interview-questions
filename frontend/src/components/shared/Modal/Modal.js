import React from 'react';
import SmallCloseIconLight from 'Assets/light/small-close-icon-light.svg';
import SmallCloseIconDark from 'Assets/dark/small-close-icon-dark.svg';
import styles from './styles.less';

const Modal = ({ mode, children, onClickCancel }) => (
  <div styleName="modal-background">
    <div styleName="box">
      <img
        src={mode === 'light' ? SmallCloseIconLight : SmallCloseIconDark}
        styleName="close"
        onClick={onClickCancel}
      />
      {children}
    </div>
  </div>
);

export default Modal;
