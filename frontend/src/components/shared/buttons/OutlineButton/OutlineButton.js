import React from 'react';
import './styles.less';

const OutlineButton = ({ label, disabled, onClickMethod }) => (
  <div styleName="container">
    <button styleName="outline-button" disabled={disabled} onClick={onClickMethod}>
      {label}
    </button>
  </div>
);

export default OutlineButton;
