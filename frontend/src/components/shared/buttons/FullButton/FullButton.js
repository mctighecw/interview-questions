import React from 'react';
import './styles.less';

const FullButton = ({ label, disabled, onClickMethod }) => (
  <div styleName="container">
    <button styleName="full-button" disabled={disabled} onClick={onClickMethod}>
      {label}
    </button>
  </div>
);

export default FullButton;
