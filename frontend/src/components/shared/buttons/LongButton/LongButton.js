import React from 'react';
import './styles.less';

const LongButton = ({ label, disabled, onClickMethod }) => (
  <div styleName="container">
    <button styleName="long-button" disabled={disabled} onClick={onClickMethod}>
      {label}
    </button>
  </div>
);

export default LongButton;
