import React from 'react';
import LongButton from 'Components/shared/buttons/LongButton/LongButton';
import './styles.less';

const NavigationRow = (props) => {
  const { dataLength, handleChangeQuestion } = props;
  const disabled = dataLength <= 1;

  return (
    <div styleName="navigation-row">
      <LongButton label="Back" disabled={disabled} onClickMethod={() => handleChangeQuestion('back')} />
      <div styleName="separator" />
      <LongButton label="Next" disabled={disabled} onClickMethod={() => handleChangeQuestion('next')} />
    </div>
  );
};

export default NavigationRow;
