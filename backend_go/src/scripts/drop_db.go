package main

import (
  "../config"
  "../db"
  "../lib"

  "go.mongodb.org/mongo-driver/bson"
)

var (
  DbName = config.DB_CONFIG.DbName
  CollUsers = config.DB_CONFIG.CollUsers
  CollLoginInfo = config.DB_CONFIG.CollLoginInfo
  CollQuestions = config.DB_CONFIG.CollQuestions
)

func main() {
  if config.APP_ENV == "development" {
    client, ctx := db.DbConnect()
    db := client.Database(DbName)
    coll1 := db.Collection(CollUsers)
    coll2 := db.Collection(CollLoginInfo)
    coll3 := db.Collection(CollQuestions)

    f := bson.M{}
    _, err1 := coll1.DeleteMany(ctx, f)
    _, err2 := coll2.DeleteMany(ctx, f)
    _, err3 := coll3.DeleteMany(ctx, f)

    if err1 != nil || err2 != nil || err3 != nil {
      lib.PrintMessage("Error dropping database " + DbName)
    } else {
      lib.PrintMessage("OK, database has been dropped")
    }
  } else {
    lib.PrintMessage("Cannot drop production database")
  }
}
