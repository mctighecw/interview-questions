package config

import (
  "os"
)

type (
  App struct {
    Port uint
  }

  Db struct {
    Host string
    Port uint
    User string
    Pw string
    AdminDb string
    DbName string
    CollUsers string
    CollLoginInfo string
    CollQuestions string
  }
)

var (
  APP_CONFIG *App
  DB_CONFIG *Db

  APP_ENV string
  SECRET_KEY string

  DB_HOST string
  DB_USER string
  DB_PASSWORD string
  DB_AUTH string
  DB_NAME string
  COLL_USERS string
  COLL_LOGIN_INFO string
  COLL_QUESTIONS string
)

func initConfig() {
  APP_ENV = os.Getenv("APP_ENV")
  SECRET_KEY = os.Getenv("SECRET_KEY")

  DB_HOST = os.Getenv("DB_HOST")
  DB_USER = os.Getenv("DB_USER")
  DB_PASSWORD = os.Getenv("DB_PASSWORD")
  DB_AUTH = os.Getenv("DB_AUTH")
  DB_NAME = os.Getenv("DB_NAME")
  COLL_USERS = os.Getenv("COLL_USERS")
  COLL_LOGIN_INFO = os.Getenv("COLL_LOGIN_INFO")
  COLL_QUESTIONS = os.Getenv("COLL_QUESTIONS")

  APP_CONFIG = &App {
    Port: 7600,
  }

  DB_CONFIG = &Db {
    Host: DB_HOST,
    Port: 27017,
    User: DB_USER,
    Pw: DB_PASSWORD,
    AdminDb: DB_AUTH,
    DbName: DB_NAME,
    CollUsers: COLL_USERS,
    CollLoginInfo: COLL_LOGIN_INFO,
    CollQuestions: COLL_QUESTIONS,
  }
}

func init() {
  initConfig()
}
