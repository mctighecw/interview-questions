package config

import (
  "github.com/form3tech-oss/jwt-go"
  "github.com/gofiber/jwt"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

var (
  JWT_CONFIG jwtware.Config
)

type JwtCustomClaims struct {
  ID primitive.ObjectID `json:"id"`
  Email string `json:"email"`
  jwt.StandardClaims
}

func init() {
  JWT_CONFIG = jwtware.Config{
    TokenLookup: "access_token_cookie",
    Claims: &JwtCustomClaims{},
    SigningKey: []byte(SECRET_KEY),
  }
}
