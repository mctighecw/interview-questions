package models

import (
  "time"

  "go.mongodb.org/mongo-driver/bson/primitive"
)

type LoginInfo struct {
  ID primitive.ObjectID `bson:"_id" json:"id,omitempty"`
  UserId string `bson:"user_id" json:"user_id"`
  UserAgent string `bson:"user_agent" json:"user_agent"`
  Language string `bson:"language" json:"language"`
  Ip string `bson:"ip" json:"ip"`
  Isp string `bson:"isp" json:"isp"`
  ConnectionType string `bson:"connection_type" json:"connection_type"`
  City string `bson:"city" json:"city"`
  Region string `bson:"region" json:"region"`
  Country string `bson:"country" json:"country"`
  TimeZone string `bson:"time_zone" json:"time_zone"`
  LoginTimestamp time.Time `bson:"login_timestamp" json:"login_timestamp,omitempty"`
}
