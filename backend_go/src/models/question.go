package models

import (
  "time"

  "go.mongodb.org/mongo-driver/bson/primitive"
)

type Question struct {
  ID primitive.ObjectID `bson:"_id" json:"id,omitempty"`
  QuestionId string `bson:"question_id" json:"question_id"`
  NumberOfRatings int `bson:"nr_ratings" json:"nr_ratings"`
  OverallRating float32 `bson:"overall_rating" json:"overall_rating"`
  LastUpdated time.Time `bson:"last_updated" json:"last_updated,omitempty"`
}
