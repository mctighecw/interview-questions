package models

import (
  "time"

  "go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
  ID primitive.ObjectID `bson:"_id" json:"id,omitempty"`
  FirstName string `bson:"first_name" json:"first_name"`
  LastName string `bson:"last_name" json:"last_name"`
  Email string `bson:"email" json:"email"`
  Password string `bson:"password" json:"password"`
  CompletedQuestions []string `bson:"completed_questions" json:"completed_questions"`
  RatedQuestions []string `bson:"rated_questions" json:"rated_questions"`
  Deleted bool `bson:"deleted" json:"deleted"`
  NrLogins int `bson:"nr_logins" json:"nr_logins"`
  LastLogin time.Time `bson:"last_login" json:"last_login,omitempty"`
  RegisteredAt time.Time `bson:"registered_at" json:"registered_at,omitempty"`
  DeletedAt time.Time `bson:"deleted_at" json:"deleted_at,omitempty"`
}
