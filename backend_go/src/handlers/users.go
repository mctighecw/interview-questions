package handlers

import (
  "../db"
  "../lib"
  "../models"

  "github.com/gofiber/fiber"
  "go.mongodb.org/mongo-driver/bson"
)

func AllUsers(c *fiber.Ctx) error {
  // Get basic info for all users from database.

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollUsers)

  u := models.User{}
  res := []models.User{}
  cursor, err := coll.Find(ctx, bson.M{})
  lib.LogError(err)

  for cursor.Next(ctx) {
    cursor.Decode(&u)
    res = append(res, u)
  }

  e := []interface{}{}

  for _, user := range res {
    f := map[string]interface{}{
      "id": user.ID,
      "email": user.Email,
      "first_name": user.FirstName,
      "last_name": user.LastName,
      "deleted": user.Deleted,
      "nr_logins": user.NrLogins,
      "last_login": user.LastLogin,
      "deleted_at": user.DeletedAt,
      "registered_at": user.RegisteredAt,
    }

    e = append(e, f)
  }

  r := map[string]interface{}{"status": "OK", "users": e}
  return c.JSON(r)
}

func OneUser(c *fiber.Ctx) error {
  // Get detailed information for one user with email.

  type request struct {
    Email string `json:"email"`
  }

  var body request
  err := c.BodyParser(&body)

  if err != nil {
    return lib.ReturnJsonParseError(c)
  }

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollUsers)

  res := coll.FindOne(ctx, bson.M{"email": body.Email})
  err2 := res.Err()

  var r = map[string]interface{}{}

  if err2 != nil {
    r = map[string]interface{}{"status": "Error", "message": "User not found"}
  } else {
    user := models.User{}
    res.Decode(&user)

    e := map[string]interface{}{
      "id": user.ID,
      "email": user.Email,
      "first_name": user.FirstName,
      "last_name": user.LastName,
      "completed_questions": user.CompletedQuestions,
      "rated_questions": user.RatedQuestions,
      "deleted": user.Deleted,
      "nr_logins": user.NrLogins,
      "last_login": user.LastLogin,
      "deleted_at": user.DeletedAt,
      "registered_at": user.RegisteredAt,
    }

    r = map[string]interface{}{"status": "OK", "user": e}
  }

  return c.JSON(r)
}
