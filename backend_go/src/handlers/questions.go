package handlers

import (
  "../db"
  "../lib"
  "../models"

  "github.com/gofiber/fiber"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

func RateQuestion(c *fiber.Ctx) error {
  // Add a user rating to a question.

  authorized, id, _ := lib.ParseCookie(c)

  if !authorized {
    return lib.ReturnUnauthorized(c)
  }

  type request struct {
    QuestionId string `json:"id"`
    Rating int `json:"rating"`
  }

  var body request
  err := c.BodyParser(&body)

  if err != nil {
    return lib.ReturnJsonParseError(c)
  }

  userId, _ := primitive.ObjectIDFromHex(id)
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  collUsers := db.Collection(CollUsers)

  // Find user
  findRes := collUsers.FindOne(ctx, bson.M{"_id": userId})
  err2 := findRes.Err()
  lib.LogError(err2)

  user := models.User{}
  findRes.Decode(&user)

  updatedSlice := user.RatedQuestions
  questionId := body.QuestionId
  userRating := body.Rating
  notPresent := true

  // Check if user has already rated question
  for i := range updatedSlice {
    if updatedSlice[i] == questionId {
      notPresent = false
      break
    }
  }

  var res interface{}

  if notPresent {
    updatedSlice = append(updatedSlice, questionId)

    // Update user in database
    _, err2 := collUsers.UpdateOne(
      ctx,
      bson.M{"_id": userId},
      bson.M{
        "$set": bson.M{
          "rated_questions": updatedSlice,
        },
      },
    )
    lib.LogError(err2)

    // Check if question in database
    collQuestions := db.Collection(CollQuestions)
    findRes := collQuestions.FindOne(ctx, bson.M{"question_id": questionId})
    err3 := findRes.Err()
    lib.LogError(err3)

    var dbError bool
    var question models.Question

    if err3 != nil {
      // Add question with rating to database
      question = models.Question{
        ID: primitive.NewObjectID(),
        QuestionId: questionId,
        NumberOfRatings: 1,
        OverallRating: float32(userRating),
      }

      _, err4 := collQuestions.InsertOne(ctx, question)
      lib.LogError(err4)

      if err4 != nil {
        dbError = true
      }
    } else {
      // Update question in database
      findRes.Decode(&question)

      nrRatings, overallRating := lib.CalculateNewRating(
        userRating, question.NumberOfRatings, question.OverallRating)

      updateRes, err4 := collQuestions.UpdateOne(
        ctx,
        bson.M{"question_id": questionId},
        bson.M{
          "$set": bson.M{
            "nr_ratings": nrRatings,
            "overall_rating": overallRating,
          },
        },
      )
      lib.LogError(err4)
      modCount := updateRes.ModifiedCount

      question = models.Question{
        NumberOfRatings: nrRatings,
        OverallRating: overallRating,
      }

      if err4 != nil || modCount == 0 {
        dbError = true
      }
    }

    if dbError {
      res = lib.ReturnError("Error rating question")
    } else {
      rating := map[string]interface{}{}
      rating[questionId] = userRating

      res = map[string]interface{}{
        "status": "OK",
        "rated_questions": updatedSlice,
        "rating": map[string]interface{}{
          "nr_ratings": question.NumberOfRatings,
          "overall_rating": lib.RoundFloat(question.OverallRating),
        },
      }
    }
  } else {
    res = lib.ReturnError("User has already rated this question")
  }

  return c.JSON(res)
}

func CompleteQuestion(c *fiber.Ctx) error {
  // Toggle a question as completed or not completed.

  authorized, id, _ := lib.ParseCookie(c)

  if !authorized {
    return lib.ReturnUnauthorized(c)
  }

  type request struct {
    QuestionId string `json:"id"`
  }

  var body request
  err := c.BodyParser(&body)

  if err != nil {
    return lib.ReturnJsonParseError(c)
  }

  userId, _ := primitive.ObjectIDFromHex(id)
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollUsers)

  // Find user
  findRes := coll.FindOne(ctx, bson.M{"_id": userId})
  err2 := findRes.Err()
  lib.LogError(err2)

  user := models.User{}
  findRes.Decode(&user)

  updatedSlice := user.CompletedQuestions
  notPresent := true

  for i := range updatedSlice {
    if updatedSlice[i] == body.QuestionId {
      updatedSlice = append(updatedSlice[:i], updatedSlice[i + 1:]...)
      notPresent = false
      break
    }
  }

  if notPresent {
    updatedSlice = append(updatedSlice, body.QuestionId)
  }

  // Update database
  updateRes, err2 := coll.UpdateOne(
    ctx,
    bson.M{"_id": userId},
    bson.M{
      "$set": bson.M{
        "completed_questions": updatedSlice,
      },
    },
  )
  lib.LogError(err2)

  modCount := updateRes.ModifiedCount
  var res interface{}

  if modCount == 0 {
    res = lib.ReturnError("Error updating completed questions")
  } else {
    res = map[string]interface{}{
      "status": "OK",
      "completed_questions": updatedSlice,
    }
  }

  return c.JSON(res)
}

func CompletedQuestions(c *fiber.Ctx) error {
  // Get slice of completed questions.

  authorized, id, _ := lib.ParseCookie(c)

  if !authorized {
    return lib.ReturnUnauthorized(c)
  }

  userId, _ := primitive.ObjectIDFromHex(id)
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollUsers)

  findRes := coll.FindOne(ctx, bson.M{"_id": userId})
  err2 := findRes.Err()
  lib.LogError(err2)

  user := models.User{}
  findRes.Decode(&user)

  res := map[string]interface{}{
    "status": "OK",
    "completed_questions": user.CompletedQuestions,
  }

  return c.JSON(res)
}

func AllQuestions(c *fiber.Ctx) error {
  // Get all questions with a rating.

  authorized, _, _ := lib.ParseCookie(c)

  if !authorized {
    return lib.ReturnUnauthorized(c)
  }

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollQuestions)

  q := models.Question{}
  r := []models.Question{}
  cursor, err := coll.Find(ctx, bson.M{})
  lib.LogError(err)

  for cursor.Next(ctx) {
    cursor.Decode(&q)
    r = append(r, q)
  }

  res := map[string]interface{}{
    "status": "OK",
    "questions": r,
  }

  return c.JSON(res)
}

func AllQuestionRatings(c *fiber.Ctx) error {
  // Get all question ratings.
  // Return a map with QuestionId as key.

  authorized, _, _ := lib.ParseCookie(c)

  if !authorized {
    return lib.ReturnUnauthorized(c)
  }

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollQuestions)

  q := models.Question{}
  r := []models.Question{}
  cursor, err := coll.Find(ctx, bson.M{})
  lib.LogError(err)

  for cursor.Next(ctx) {
    cursor.Decode(&q)
    r = append(r, q)
  }

  type questionData struct {
    NumberOfRatings int `json:"nr_ratings"`
    OverallRating float32 `json:"overall_rating"`
  }

  d := make(map[string]interface{})

  for _, el := range r {
    var k string = el.QuestionId
    v := &questionData{
      NumberOfRatings: el.NumberOfRatings,
      OverallRating: lib.RoundFloat(el.OverallRating),
    }
    d[k] = v
  }

  res := map[string]interface{}{
    "status": "OK",
    "data": d,
  }

  return c.JSON(res)
}
