package handlers

import (
  "../config"
  "../db"
  "../lib"
  "../models"

  "time"

  "github.com/form3tech-oss/jwt-go"
  "github.com/gofiber/fiber"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

var (
  DbName = config.DB_CONFIG.DbName
  CollUsers = config.DB_CONFIG.CollUsers
  CollLoginInfo = config.DB_CONFIG.CollLoginInfo
  CollQuestions = config.DB_CONFIG.CollQuestions
)

func Register(c *fiber.Ctx) error {
  // Registers a new user and creates new database fields.

  type request struct {
    FirstName string `json:"first_name"`
    LastName string `json:"last_name"`
    Email string `json:"email"`
    Password string `json:"password"`
  }

  var body request
  err := c.BodyParser(&body)

  if err != nil {
    return lib.ReturnJsonParseError(c)
  }

  firstName := body.FirstName
  lastName := body.LastName
  email := body.Email
  password := body.Password

  var res interface{}

  if firstName != "" && lastName != "" && email != "" && password != "" {
    client, ctx := db.DbConnect()
    db := client.Database(DbName)
    coll := db.Collection(CollUsers)

    findRes := coll.FindOne(ctx, bson.M{"email": email})
    err2 := findRes.Err()

    if err2 != nil {
      hashedPassword := lib.HashPassword(password)

      u := models.User{
        ID: primitive.NewObjectID(),
        FirstName: firstName,
        LastName: lastName,
        Email: email,
        CompletedQuestions: []string{},
        RatedQuestions: []string{},
        Password: hashedPassword,
        RegisteredAt: time.Now(),
      }

      _, err3 := coll.InsertOne(ctx, u)
      lib.LogError(err)

      if err3 != nil {
        res = lib.ReturnError("An error has occurred registering user")
        return c.Status(500).JSON(res)
      } else {
        res = lib.ReturnOK("New user has successfully registered")
        return c.Status(200).JSON(res)
      }
    } else {
      res = lib.ReturnError("Email address is already taken")
      return c.Status(409).JSON(res)
    }
  } else {
    res = lib.ReturnError("Please provide all required fields")
    return c.Status(400).JSON(res)
  }
}

func Login(c *fiber.Ctx) error {
  // Logs the user in and saves login info (browser info, location, etc.).

  type request struct {
    Email string `json:"email"`
    Password string `json:"password"`
    LoginInfo models.LoginInfo `json:"login_info"`
  }

  var body request
  err := c.BodyParser(&body)

  if err != nil {
    return lib.ReturnJsonParseError(c)
  }

  email := body.Email
  password := body.Password
  loginInfo := body.LoginInfo

  var res interface{}

  if email != "" && password != "" {
    client, ctx := db.DbConnect()
    db := client.Database(DbName)
    collUsers := db.Collection(CollUsers)

    findRes := collUsers.FindOne(ctx, bson.M{"email": email})
    err2 := findRes.Err()

    user := models.User{}
    findRes.Decode(&user)
    pwValid := lib.CheckPasswordHash(password, user.Password)

    if err2 == nil && pwValid {
      // Save login info to database
      userId := (user.ID).Hex()

      i := models.LoginInfo{
        ID: primitive.NewObjectID(),
        UserId: userId,
        UserAgent: loginInfo.UserAgent,
        Language: loginInfo.Language,
        Ip: loginInfo.Ip,
        Isp: loginInfo.Isp,
        ConnectionType: loginInfo.ConnectionType,
        City: loginInfo.City,
        Region: loginInfo.Region,
        Country: loginInfo.Country,
        TimeZone: loginInfo.TimeZone,
        LoginTimestamp: time.Now(),
      }

      collLoginInfo := db.Collection(CollLoginInfo)
      _, err3 := collLoginInfo.InsertOne(ctx, i)
      lib.LogError(err3)

      // Update user in database
      _, err4 := collUsers.UpdateOne(
        ctx,
        bson.M{"_id": user.ID},
        bson.M{
          "$set": bson.M{
            "nr_logins": user.NrLogins + 1,
            "last_login": time.Now(),
          },
        },
      )
      lib.LogError(err4)

      // Create JWT
      id := user.ID
      expiration := time.Now().Add(time.Hour * 72)

      claims := &config.JwtCustomClaims{
        id,
        email,
        jwt.StandardClaims{
          ExpiresAt: expiration.Unix(),
        },
      }
      token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
      t, err5 := token.SignedString([]byte(config.SECRET_KEY))
      lib.LogError(err5)

      // Create cookie with JWT
      secure := config.APP_ENV == "production"
      cookie := &fiber.Cookie{
        Name: "access_token_cookie",
        Value: t,
        Expires: expiration,
        Path: "/api/",
        HTTPOnly: true,
        Secure: secure,
      }

      res = lib.ReturnUserInfo(user)
      c.Cookie(cookie)
      return c.Status(200).JSON(res)
    } else {
      res = lib.ReturnError("Wrong login credentials")
      return c.Status(401).JSON(res)
    }

  } else {
    res = lib.ReturnError("Please provide all required fields")
    return c.Status(400).JSON(res)
  }
}

func Logout(c *fiber.Ctx) error {
  // Logs the user out.

  c.Cookie(&fiber.Cookie{
    Name: "access_token_cookie",
    Path: "/api/",
    Expires: time.Now().Add(-(time.Hour * 2)),
  })

  m := "User logged out"
  res := lib.ReturnOK(m)
  return c.Status(200).JSON(res)
}
