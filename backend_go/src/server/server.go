package server

import (
  "../handlers"
  "../lib"

  "github.com/gofiber/fiber"
  "github.com/gofiber/fiber/middleware/cors"
  "github.com/gofiber/fiber/middleware/logger"
)

func CreateServer() *fiber.App {
  app := fiber.New()

  // Default logger
  app.Use(logger.New())

  // CORS in development
  if lib.GetEnv("APP_ENV", "production") == "development" {
    app.Use(cors.New(cors.Config{
      AllowCredentials: true,
    }))
  }

  // Routing
  api := app.Group("/api")

  // Auth routes
  auth := api.Group("/auth")
  auth.Post("/register", handlers.Register)
  auth.Post("/login", handlers.Login)
  auth.Get("/logout", handlers.Logout)

  // Users routes
  users := api.Group("/users")
  users.Get("/all", handlers.AllUsers)
  users.Post("/one", handlers.OneUser)

  // Questions routes
  questions := api.Group("/questions")
  questions.Post("/rate", handlers.RateQuestion)
  questions.Post("/complete", handlers.CompleteQuestion)
  questions.Get("/completed", handlers.CompletedQuestions)
  questions.Get("/ratings", handlers.AllQuestionRatings)
  questions.Get("/all", handlers.AllQuestions)

  return app
}
