package main

import (
  "./config"
  "./server"

  "fmt"
)

func main() {
  app := server.CreateServer()
  port := fmt.Sprintf(":%d", config.APP_CONFIG.Port)

  app.Listen(port)
}
