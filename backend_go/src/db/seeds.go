package db

import (
  "../lib"
  "../models"

  "time"

  "go.mongodb.org/mongo-driver/bson/primitive"
)

var (
  U1 = models.User{
    ID: primitive.NewObjectID(),
    FirstName: "John",
    LastName: "Smith",
    Email: "john@smith.com",
    Password: lib.HashPassword("john1"),
    CompletedQuestions: []string{},
    RatedQuestions: []string{},
    Deleted: false,
    RegisteredAt: time.Now(),
  }

  U2 = models.User{
    ID: primitive.NewObjectID(),
    FirstName: "Susan",
    LastName: "Williams",
    Email: "susan@williams.com",
    Password: lib.HashPassword("susan1"),
    CompletedQuestions: []string{},
    RatedQuestions: []string{},
    Deleted: false,
    RegisteredAt: time.Now(),
  }

  U3 = models.User{
    ID: primitive.NewObjectID(),
    FirstName: "Sam",
    LastName: "Jones",
    Email: "sam@jones.com",
    Password: lib.HashPassword("sam1"),
    CompletedQuestions: []string{},
    RatedQuestions: []string{},
    Deleted: false,
    RegisteredAt: time.Now(),
  }
)
