package lib

import (
  "../models"

  "github.com/gofiber/fiber"
)

func ReturnError(m string) interface{} {
  r := map[string]interface{}{"status": "Error", "message": m}
  return r
}

func ReturnOK(m string) interface{} {
  r := map[string]interface{}{"status": "OK", "message": m}
  return r
}

func ReturnUserInfo(user models.User) interface{} {
  r := map[string]interface{}{
    "status": "OK",
    "message": "User logged in",
    "user_info": map[string]interface{}{
      "email": user.Email,
      "first_name": user.FirstName,
      "last_name": user.LastName,
      "registered_at": user.RegisteredAt,
    },
    "completed_questions": user.CompletedQuestions,
    "rated_questions": user.RatedQuestions,
  }
  return r
}

func ReturnJsonParseError(c *fiber.Ctx) error {
  return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
    "status": "Error",
    "message": "Cannot parse request JSON",
  })
}

func ReturnUnauthorized(c *fiber.Ctx) error {
  return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
    "status": "Unauthorized",
    "message": "JWT is missing or invalid",
  })
}
