package lib

import (
  "../config"

  "github.com/form3tech-oss/jwt-go"
  "github.com/gofiber/fiber"
  "golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) string {
  bytes, _ := bcrypt.GenerateFromPassword([]byte(password), 14)
  return string(bytes)
}

func CheckPasswordHash(password string, hash string) bool {
  err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
  return err == nil
}

func ParseCookie(c *fiber.Ctx) (bool, string, string) {
  // Check authorization by presence of access_token_cookie.
  // If authorized, parse cookie and return user id and email values.

  tokenString := c.Cookies("access_token_cookie")
  claims := jwt.MapClaims{}
  _, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
    return []byte(config.SECRET_KEY), nil
  })

  if err != nil {
    return false, "", ""
  }

  id := claims["id"].(string)
  email := claims["email"].(string)

  return true, id, email
}
