package lib

import (
  "fmt"
  "math"
  "os"
)

func PrintMessage(m string) {
  fmt.Println(m)
}

func LogError(err error) {
  if err != nil {
    fmt.Println(err)
    return
  }
}

func ErrorPanic(err error) {
  if err != nil {
    panic(err)
  }
}

func GetEnv(key, fallback string) string {
  if value, ok := os.LookupEnv(key); ok {
    return value
  }
  return fallback
}

func GetRootDir() string {
  dir, err := os.Getwd()
  LogError(err)
  return dir
}

func CalculateNewRating(rating int, nrRatings int, overallRating float32) (int, float32) {
  newNrRatings := nrRatings + 1
  total := float32(overallRating) * float32(nrRatings)
  newTotal := total + float32(rating)
  newOverallRating := newTotal / float32(newNrRatings)

  return newNrRatings, newOverallRating
}

func RoundFloat(nr float32) float32 {
  // Round float32 to max. 3 decimal places
  nr64 := float64(nr)
  res := math.Round(nr64 * 1000) / 1000
  return float32(res)
}
