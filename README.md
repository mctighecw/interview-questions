# README

This is an app to help frontend developers prepare for technical interviews during the job application process.

Users can choose from various categories (general, JavaScript, React, styling, etc.) and practice answering frequently asked questions. When users register, they can mark which questions they have already answered and also give the question a rating.

There are two backends to choose from: one in Python with the framework Flask, the other in Go with the framework [Fiber](https://gofiber.io/). (This is my first project using the latter.)

The questions data are located in a separate (private) repository, which has been added as a git submodule.

## App Information

App Name: interview-questions

Created: October 2020 - January 2021

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/interview-questions)

Production: [Link](https://interview-questions.mctighecw.site)

## Tech Stack

Frontend:

- React
- Redux
- Less
- CSS Modules
- Webpack
- Prettier

Backend:

- Two options:
  - Python & Flask _or_
  - Go & Fiber
- MongoDB
- JWT Authentication
- REST APIs

## To Run (development)

1. Build

   ```sh
   $ docker-compose -f docker-compose-dev.yml up -d --build
   ```

2. Setup database

   ```sh
   $ docker exec -it interview-questions_mongodb_1 bash
     (create admin user)
   $ exit
   $ source init_docker_db.sh
   ```

App running at `localhost:3000`

## Submodule Update

The interview questions data is in a separate submodule. If it gets changed, update the submodule:

```sh
$ git pull --recurse-submodules
$ git submodule update --remote
```

## Credits

- Day/Night mode switch adapted from this [Codepen](https://codepen.io/jsndks/pen/qEXzOQ)
- Burger menu icon courtesy of Freepik at [Flaticon](https://www.flaticon.com/authors/freepik)
- Close icon courtesy of Freepik at [Flaticon](https://www.flaticon.com/authors/freepik)
- Logout icon courtesy of Freepik at [Flaticon](https://www.flaticon.com/authors/freepik)
- Favicon ? icon courtesy of Freepik at [Flaticon](https://www.flaticon.com/authors/freepik)

Last updated: 2025-02-06
